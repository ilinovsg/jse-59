package ru.ilinovsg.tm.jse59.config;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.ilinovsg.tm.jse59.model.Bus;
import ru.ilinovsg.tm.jse59.model.ElectricCar;
import ru.ilinovsg.tm.jse59.model.SportsCar;
import ru.ilinovsg.tm.jse59.model.Truck;
import ru.ilinovsg.tm.jse59.model.Vehicle;

public class HibernateConfig {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfg = new Configuration();
        cfg.addAnnotatedClass(Bus.class);
        cfg.addAnnotatedClass(ElectricCar.class);
        cfg.addAnnotatedClass(SportsCar.class);
        cfg.addAnnotatedClass(Truck.class);
        cfg.addAnnotatedClass(Vehicle.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfg.getProperties())
                .build();

        sessionFactory = cfg.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
