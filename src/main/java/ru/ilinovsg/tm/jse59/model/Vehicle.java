package ru.ilinovsg.tm.jse59.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "vehicle")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String brand;

    private String model;

    @Column(name = "year", columnDefinition = "TIMESTAMP")
    private LocalDate year;

    public Vehicle(String brand, String model, LocalDate year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
    }
}

