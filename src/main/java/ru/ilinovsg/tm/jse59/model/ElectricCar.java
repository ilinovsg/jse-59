package ru.ilinovsg.tm.jse59.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "electric_car")
public class ElectricCar extends Vehicle{
    private Integer max_distance;

    public ElectricCar(String brand, String model, LocalDate year, Integer max_distance) {
        super(brand, model, year);
        this.max_distance = max_distance;
    }

    @Override
    public String toString() {
        return "ElectricCar{" +
                "max_distance=" + max_distance +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
