package ru.ilinovsg.tm.jse59.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "truck")
public class Truck extends Vehicle{
    private Integer carrying;
    public Truck(String brand, String model, LocalDate year, Integer carrying) {
        super(brand, model, year);
        this.carrying = carrying;
    }

    @Override
    public String toString() {
        return "Truck{" +
                "carrying=" + carrying +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
