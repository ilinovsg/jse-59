package ru.ilinovsg.tm.jse59.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "bus")
public class Bus extends Vehicle{
    private Integer max_passengers;

    public Bus(String brand, String model, LocalDate year, Integer max_passengers) {
        super(brand, model, year);
        this.max_passengers = max_passengers;
    }

    @Override
    public String toString() {
        return "Bus{" +
                "max_passengers=" + max_passengers +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
