package ru.ilinovsg.tm.jse59.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import java.time.LocalDate;

@Entity
@Table(name = "sports_car")
public class SportsCar extends Vehicle{
    private Integer horsepower;

    private Integer acceleration;

    public SportsCar(String brand, String model, LocalDate year, Integer horsepower, Integer acceleration) {
        super(brand, model, year);
        this.horsepower = horsepower;
        this.acceleration = acceleration;
    }

    @Override
    public String toString() {
        return "SportsCar{" +
                "horsepower=" + horsepower +
                ", acceleration=" + acceleration +
                ", fromSuper=" + super.toString() +
                '}';
    }
}
