package ru.ilinovsg.tm.jse59;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.ilinovsg.tm.jse59.config.HibernateConfig;
import ru.ilinovsg.tm.jse59.model.Bus;
import ru.ilinovsg.tm.jse59.model.ElectricCar;
import ru.ilinovsg.tm.jse59.model.SportsCar;
import ru.ilinovsg.tm.jse59.model.Truck;
import ru.ilinovsg.tm.jse59.model.Vehicle;

public class Main {
    public static void main(String[] args) {

        var sportsCar = new SportsCar("Dodge", "Viper", LocalDate.ofYearDay(2017, 1), 400, 4);
        var bus = new Bus("Volvo", "9700", LocalDate.ofYearDay(2001, 3), 52);
        var electricCar = new ElectricCar("Tesla", "X", LocalDate.ofYearDay(2015, 2), 400);
        var truck = new Truck("Man", "TGX", LocalDate.ofYearDay(2020, 1), 23000);

        SessionFactory sessionFactory = HibernateConfig.getSessionFactory();

        Session session;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.persist(sportsCar);
            session.persist(bus);
            session.persist(electricCar);
            session.persist(truck);
            transaction.commit();
        } catch (Exception e){
            transaction.rollback();
            throw e;
        }

        try{
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            List<Vehicle> vehicles = session.createQuery("select v from Vehicle v").list();
            for (int i = 0; i < vehicles.size(); i++){
                System.out.println(vehicles.get(i));
            }
        } catch (Exception e){{
            transaction.rollback();
            throw e;
        }}
    }
}

